package SystemLibrary;

import Model2.Media;
import Model2.ReferBook;
import SystemLibrary.Library;
import Model1.Officer;
import Model1.Student;
import Model1.Teacher;

public class Main {
	public static void main(String[] args){
		Student st1;
		Teacher tc1;
		Officer off1;
		Library lib = new Library();
		
		Media book1 = new Media("Big Java", 4,"Jason");//bookname,Amount borrow,borrower
		ReferBook book2 = new ReferBook("PPL", 500,"Umaporn");//
		lib.addBook(book1);
		lib.addBook(book2);
		
		st1 = new Student();
		st1.setName("Thana Srivakoen");
		st1.setStuNum(5610411);
		st1.setTypeName("Student");
		st1.setborrowCount(3);
		
		tc1 = new Teacher();
		tc1.setName("Usa Sammapan");
		tc1.setTeachNum(56751);
		tc1.setTypeName("Teacher");
		tc1.setborrowCount(5);
		
		off1 = new Officer();
		off1.setName("Kamiko Ononai");
		off1.setTypeName("Officer");
		off1.setborrowCount(5);
		
		System.out.println(lib.bookCount(null));//3
		lib.borrowBook(st1,2);//borrow *mutator
		
		System.out.println(lib.bookCount(null));//2 *assesor
		System.out.println(st1.getBorrowerNum());//Big Java
		System.out.println(lib.getBorrower());//mak jacobsen
		System.out.println(st1.getName());//mak jacobsen
		System.out.println(lib.bookCount(null));
		
		lib.returnBook(st1,"Big Java");//bookname *mutator
		System.out.println(lib.bookCount(null));//3
		System.out.println(lib.getBorrower());//empty
	}
}

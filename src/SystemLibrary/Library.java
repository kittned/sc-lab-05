package SystemLibrary;

import java.util.ArrayList;

import javax.print.attribute.standard.Media;

import Model1.Officer;
import Model1.Student;
import Model1.Teacher;
import Model2.ReferBook;
import Model2.Textbook;

public class Library<Int> {
	private Student borrower;
	private String name;
	private int borrow;
		
	public Library(){
		name = "Unknown";
		borrow = 0;
	}
	
	public String getName(){
		return name;
	}
	public void setName(String mediaName){
		name = mediaName;
	}
	
	public void returnBook(int giveback){
		borrow = borrow - giveback;
	}
	
	public void borrowBook(Student stu, int borrow) {
		// TODO Auto-generated method stub
		this.borrow = borrow;
	}
	
	public static void addBook(Model2.Media book1) {
		// TODO Auto-generated method stub
		Library.addBook(book1);
	}

	public static void addBook(ReferBook book2) {
		// TODO Auto-generated method stub
		Library.addBook(book2);
	}

	public static void addBook(Textbook text) {
		// TODO Auto-generated method stub
		Library.addBook(text);
	}
	
	public char[] bookCount(Media book) {
		// TODO Auto-generated method stub
		ArrayList<Int> list = new ArrayList<Int>();
		list.add((Int) book);
		return null;
	}

	public String getBorrower(){
		return borrower.getName();
	}
	
	public void setBorrower(Student student){
		borrower = student;
	}
	
	public String getTypeName(){
		return borrower.getTypeName();
	}
	
	public double getBorrowerNum(){
		return borrower.getStuNum();
	}
	
	public String toString(){
		return "Borrower Name: " +borrower.getName()+ "\n" + "Type Borrower: " + borrower.getTypeName()+ "\n" + 
				"ID Number:" + borrower.getStuNum() + "\n" + "Books Borrowed: " + borrow;
	}

	public void returnBook(Student st1, String string) {
		// TODO Auto-generated method stub
		
	}
	
}

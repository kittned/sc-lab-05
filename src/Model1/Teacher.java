package Model1;

public class Teacher extends Person{
	private int teachNum;
	private int borrow;
	protected static String type;
	
	public Teacher(){
		super(type);
		teachNum= 0;
		type = "Teacher";
	}
	
	public Teacher(String initialName, int initialteachNum, String type){
		super(initialName);
		teachNum = initialteachNum;
	}
	
	public void reset(String newName, int newteachNum){
		setName(newName);
		teachNum = newteachNum;
	}
	
	public int getTeachNum(){
		return teachNum;
	}
	
	public void setTeachNum(int newteachNum){
		teachNum = newteachNum;
	}
	
	public void setTypeName(String typeName){
		type = "Student";
	}
	
	public String getTypeName(){
		return type;
	}
	
	public int getborrowCount(){
		return borrow;
	}
	
	public void setborrowCount(int borrowCount){
		borrow = borrowCount;
	}
	
	public boolean equals(Teacher otherTeacher){
		return this.hasSameName(otherTeacher)&&(this.teachNum == otherTeacher.teachNum);
	}
}

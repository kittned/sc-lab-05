package Model1;

public class Doctor extends Student{
	private int level;
	protected String type;
	
	public Doctor(){
		super();
		type = "Doctor";
		level = 1;
	}
	
	public Doctor(String initialName, int initialStuNum, int initialLevel){
		super(initialName, initialStuNum, initialName);
		setLevel(initialLevel);
	}
	
	public void reset(String newName, int newStuNum, int newLevel){
		reset(newName, newStuNum);
		setLevel(newLevel);
	}
	
	public int getlevel(){
		return level;
	}
	
	public void setLevel(int newLevel){
		if((1 <= newLevel) && (newLevel <= 3))
			level = newLevel;
		else{
			System.exit(0);
		}
	}
	
	public boolean equals(Doctor otherDoctor){
		return equals((Student)otherDoctor)&&(this.level == otherDoctor.level);
	}
}

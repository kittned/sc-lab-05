package Model1;

public class Master extends Student{
	private int level;
	protected String type;
	
	public Master(){
		super();
		type = "Master";
		level = 1;
	}
	
	public Master(String initialName, int initialStuNum, int initialLevel){
		super();
		setLevel(initialLevel);
	}
	
	public void reset(String newName, int newStuNum, int newLevel){
		reset(newName, newStuNum, newLevel);
		setLevel(newLevel);
	}
	
	public int getlevel(){
		return level;
	}
	
	public void setLevel(int newLevel){
		if((1 <= newLevel) && (newLevel <= 3))
			level = newLevel;
		else{
			System.exit(0);
		}
	}
	
	public boolean equals(Doctor otherMaster){
		return equals((Student)otherMaster)&&(this.level == otherMaster.getlevel());
	}
	
}

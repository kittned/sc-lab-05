package Model1;

public class Student extends Person{
	private double stuNum;
	private int borrow;
	protected static String type;
	
	public Student(){
		super(type);
		stuNum = 0;
		type = "Student";
	}
	
	public Student(String initialName, int initialStuNum, String type){
		super(initialName);
		stuNum = initialStuNum;
	}
	
	public void reset(String newName, int newStuNum){
		setName(newName);
		stuNum = newStuNum;
	}
	
	public double getStuNum(){
		return stuNum;
	}
	
	public void setStuNum(double newStuNum){
		stuNum = newStuNum;
	}
	
	public void setTypeName(String typeName){
		type = "Student";
	}
	
	public String getTypeName(){
		return type;
	}
	
	public int getborrowCount(){
		return borrow;
	}
	
	public void setborrowCount(int borrowCount){
		borrow = borrowCount;
	}
	
	public boolean equals(Student otherStudent){
		return this.hasSameName(otherStudent)&&(this.stuNum == otherStudent.stuNum);
	}

	public char[] getBorrowerNum() {
		// TODO Auto-generated method stub
		return null;
	}
}

package Model1;

public class Person {
	private String name;
	
	public Person(String initialName, int initialStuNum, int initialLevel){
		name = "Unknown";
	}
	
	public Person(String initialName){
		name = initialName;
	}
	
	public void setName(String newName){
		name = newName;
	}
	
	public String getName(){
		return name;
	}
	
	public boolean hasSameName(Person otherPerson){
		return this.name.equalsIgnoreCase(otherPerson.name);
	}
}

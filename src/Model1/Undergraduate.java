package Model1;

public class Undergraduate extends Student{
	private int level;
	protected String type;
	
	public Undergraduate(){
		super();
		type = "Undergraduate";
		level = 1;
	}
	
	public Undergraduate(String initialName, int initialStuNum, int initialLevel){
		super(initialName, initialStuNum, initialName);
		setLevel(initialLevel);
	}
	
	public void reset(String newName, int newStuNum, int newLevel){
		reset(newName, newStuNum);
		setLevel(newLevel);
	}
	
	public int getlevel(){
		return level;
	}
	
	public void setLevel(int newLevel){
		if((1 <= newLevel) && (newLevel <= 4))
			level = newLevel;
		else{
			System.exit(0);
		}
	}
	
	public boolean equals(Undergraduate otherUndergraduate){
		return equals((Student)otherUndergraduate)&&(this.level == otherUndergraduate.level);
	}
}

package Model1;

public class Officer extends Person{
	private int borrow;
	protected static String type;
	
	public Officer(){
		super(type);
		type = "Teacher";
	}
	
	public Officer(String initialName, String type){
		super(initialName);
	}
	
	public void reset(String newName){
		setName(newName);
	}
	
	public void setTypeName(String typeName){
		type = "Student";
	}
	
	public String getTypeName(){
		return type;
	}
	
	public int getborrowCount(){
		return borrow;
	}
	
	public void setborrowCount(int borrowCount){
		borrow = borrowCount;
	}
	
	public boolean equals(Officer otherOfficer){
		return this.hasSameName(otherOfficer);
	}
	
}
